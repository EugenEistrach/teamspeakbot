package de.eistrach.tsbot.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import java.io.File

class ConfigFactory {
    companion object {
        var configFile = File("ts3bot.yml")
            private set

        fun loadOrCreateConfig() {
            val mapper = ObjectMapper(YAMLFactory());

            configFile = File("ts3bot.yml")

            if (!configFile.exists()){
                configFile.createNewFile()
                mapper.writeValue(configFile, config)
            } else {
                config = mapper.readValue(configFile, Config::class.java)

            }
            mapper.writeValue(configFile, config)
        }

        var config = Config()
            private set
    }
}