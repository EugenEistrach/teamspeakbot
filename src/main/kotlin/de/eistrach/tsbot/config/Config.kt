package de.eistrach.tsbot.config


class Config {
    var serverConfig = ServerConfig()
    var playerCounterConfig = PlayerCounterConfig()
    var greetingConfig = GreetingConfig()
    var afkManagementConfig = AfkManagementConfig()

    var notifications = listOf(NotificationConfig())
    var commands = listOf(AddGroupCommandConfig())

    var normalCommands = NormalCommandsConfig()
    var trollCommands = TrollCommandsConfig()
}

