package de.eistrach.tsbot.config

class ServerConfig {
    var ip: String = "0.0.0.0"
    var username: String = "username"
    var password: String = "pw"
    var nickname: String = "nickname"
    var virtualServerId: Int = 1
}