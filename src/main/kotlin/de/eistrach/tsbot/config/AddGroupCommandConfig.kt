package de.eistrach.tsbot.config

class AddGroupCommandConfig {
    var enabled = false
    var commandName = "cmd1"
    var groupRequired = true
    var requiredGroup = 0
    var toggleMode = false
    var groupsToAdd = listOf(0, 0)
}