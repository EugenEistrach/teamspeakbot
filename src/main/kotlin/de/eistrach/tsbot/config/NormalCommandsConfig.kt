package de.eistrach.tsbot.config

class NormalCommandsConfig{
    var enabled = true
    var fetchClientIdCommand = CommandConfig("fetchid")
    var fetchRandomClientInChannelCommand = CommandConfig("fetchrclient")
}