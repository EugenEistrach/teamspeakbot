package de.eistrach.tsbot.config

class AfkManagementConfig{
    var enabled = true
    var afkDurationInSec = 600
    var afkChannelId = 0
}