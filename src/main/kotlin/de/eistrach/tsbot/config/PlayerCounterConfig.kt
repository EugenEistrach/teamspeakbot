package de.eistrach.tsbot.config

class PlayerCounterConfig {
    var enabled = false
    var channelId = 0
    var channelName = "Active players:  %d"
}