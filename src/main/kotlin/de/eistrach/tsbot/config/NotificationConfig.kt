package de.eistrach.tsbot.config

class NotificationConfig {
    var enabled = false
    var channelId = 0
    var groupId = 0
    var noClientsAvailableMessage = "No clients available"
    var clientsAvailableMessage = "Help is comming, please wait"
    var clientNotificationMessage = "Client %s joined channel %s, you may need to help"
}