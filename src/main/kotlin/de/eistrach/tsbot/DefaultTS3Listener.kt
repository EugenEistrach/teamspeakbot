package de.eistrach.tsbot

import com.github.theholywaffle.teamspeak3.api.ChannelProperty
import com.github.theholywaffle.teamspeak3.api.event.*
import com.github.theholywaffle.teamspeak3.api.wrapper.Client
import de.eistrach.tsbot.config.CommandConfig
import de.eistrach.tsbot.config.Config
import de.eistrach.tsbot.config.ConfigFactory
import de.eistrach.tsbot.extensions.getRandomElement

class DefaultTS3Listener : TS3Listener{



    override fun onChannelDeleted(e: ChannelDeletedEvent?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onChannelMoved(e: ChannelMovedEvent?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPrivilegeKeyUsed(e: PrivilegeKeyUsedEvent?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onChannelEdit(e: ChannelEditedEvent?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onClientJoin(e: ClientJoinEvent?) {
        if (e == null)
            return;
        refreshPlayerCountChannel()
        sendWelcomeMessage(e.clientId)
    }

    override fun onChannelDescriptionChanged(e: ChannelDescriptionEditedEvent?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onClientMoved(e: ClientMovedEvent?) {
        if (e == null)
            return;
        processNotifications(e.clientId, e.targetChannelId)
    }

    override fun onServerEdit(e: ServerEditedEvent?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onChannelCreate(e: ChannelCreateEvent?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onChannelPasswordChanged(e: ChannelPasswordChangedEvent?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onTextMessage(e: TextMessageEvent?) {
        if (e == null)
            return

        val client = TS3.api.getClientByUId(e.invokerUniqueId)

        processGroupAddingCommands(e.message, client)
        processNormalCommands(e.message, client)
        processTrollCommands(e.message, client)
    }



    override fun onClientLeave(e: ClientLeaveEvent?) {
        if (e == null)
            return;
        refreshPlayerCountChannel()
    }

    private fun sendWelcomeMessage(clientId: Int) {
        if (!ConfigFactory.config.greetingConfig.enabled)
            return

        val config = ConfigFactory.config.greetingConfig

        val clientName = TS3.api.getClientInfo(clientId).nickname
        TS3.api.sendPrivateMessage(clientId, config.message.format(clientName))
    }

    private fun refreshPlayerCountChannel(){
        val config = ConfigFactory.config.playerCounterConfig

        if (!config.enabled)
            return

        val options = HashMap<ChannelProperty, String>()
        val clients = TS3.playerCount()
        options.put(ChannelProperty.CHANNEL_NAME, config.channelName.format(clients))
        TS3.api.editChannel(config.channelId, options)
    }

    private fun processNotifications(clientId: Int, channelId: Int){
        for (nc in ConfigFactory.config.notifications){
            if (!nc.enabled || channelId != nc.channelId)
                continue
            var foundClient = false

            for (client in TS3.api.clients){
                if (client.isInServerGroup(nc.groupId)){
                    foundClient = true
                    val channel = TS3.api.getChannelInfo(channelId).name
                    val clientName = TS3.api.getClientInfo(clientId).nickname
                    TS3.api.sendPrivateMessage(client.id, nc.clientNotificationMessage.format(clientName, channel))
                }
            }
            val message = if (foundClient) nc.clientsAvailableMessage else nc.noClientsAvailableMessage
            TS3.api.sendPrivateMessage(clientId, message)
        }
    }

    private fun processGroupAddingCommands(cmd: String, client: Client){
        for (conf in ConfigFactory.config.commands){
            if (!conf.commandName.equals(cmd) || !conf.enabled)
                continue

            val remove = conf.groupsToAdd.any{ client.isInServerGroup(it)}

            if (!remove && conf.groupRequired && !client.isInServerGroup(conf.requiredGroup))
                continue

            for (group in conf.groupsToAdd)
            {
                if (conf.toggleMode && remove)
                    TS3.api.removeClientFromServerGroup(group, client.databaseId)
                else
                    TS3.api.addClientToServerGroup(group, client.databaseId)
            }
        }
    }

    private fun processNormalCommands(message: String, client: Client) {
        if (!ConfigFactory.config.normalCommands.enabled)
            return

        if (isCommand(message, ConfigFactory.config.normalCommands.fetchClientIdCommand, client))
            processChannelIdCommand(client)

        if (isCommand(message, ConfigFactory.config.normalCommands.fetchRandomClientInChannelCommand, client))
            processRandomClientInChannelCommand(client)
    }

    private fun processChannelIdCommand(client: Client){
        val channelId = client.channelId
        TS3.api.sendPrivateMessage(client.id, "ChannelId: " + channelId)
    }

    private fun processRandomClientInChannelCommand(client: Client){
        val channelId = client.channelId

        val clients = TS3.clients().filter{it.channelId == channelId && it.uniqueIdentifier != client.uniqueIdentifier}

        val rc = clients.getRandomElement()

        val clientName = rc.nickname


        val id = TS3.api.channels.find{it.isEmpty}?.id
        if (id != null)
            TS3.api.moveClient(0, id)

        TS3.api.sendChannelMessage(channelId, clientName)
    }

    private fun processTrollCommands(message: String, client: Client){
        if (!ConfigFactory.config.trollCommands.enabled)
            return

        if (client.isInServerGroup(ConfigFactory.config.trollCommands.trollGroupId))
        {
            processMoveCommand(message, client.channelId, client)
        }
    }

    private fun processMoveCommand(message: String, channelId: Int, client: Client){
        if(isCommand(message, ConfigFactory.config.trollCommands.moveCommand, client))
        {
            for (c in TS3.clients())
                TS3.api.moveClient(c.id, channelId)
        }
    }

    private fun isCommand(message: String, cmd: CommandConfig, client: Client) : Boolean {
        if (!cmd.enabled)
            return false

        if (cmd.groupRequired && !client.isInServerGroup(cmd.groupId))
            return false

        return message.equals(cmd.command)
    }
}