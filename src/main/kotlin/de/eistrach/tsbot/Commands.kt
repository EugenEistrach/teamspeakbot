package de.eistrach.tsbot

import de.eistrach.tsbot.config.ConfigFactory
import kotlin.system.exitProcess

data class Command (val command: String, val description: String, val func: ()-> Unit)

val commands = listOf(
Command("/help", "lists all commands"){
    listCommands()
},

Command("/reload", "reloads the config file but ignores the server configuration") {
    ConfigFactory.loadOrCreateConfig()
}

/*Command("/stop", "stops the server") {
    TS3.query.exit();
    exitProcess(0)
}*/
)

fun listCommands() {
    for (cmd in commands) {
        println("-" + cmd.command)
        println("\t" + cmd.description)
    }
}