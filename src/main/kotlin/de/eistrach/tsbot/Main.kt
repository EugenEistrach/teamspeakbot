package de.eistrach.tsbot

import com.github.theholywaffle.teamspeak3.api.wrapper.Client
import de.eistrach.tsbot.config.ConfigFactory
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.system.exitProcess

fun main(args: Array<String>){
    if (!ConfigFactory.configFile.exists()){
        ConfigFactory.loadOrCreateConfig()
        println("Config file generated. Please close the server, edit the file and start the server again.")
        readLine()
        exitProcess(0)
    }

    ConfigFactory.loadOrCreateConfig()

    JavaMain.Run()
    val execService = Executors.newScheduledThreadPool(5)
    execService.scheduleAtFixedRate({ run { update() } }, 0L, 1L, TimeUnit.SECONDS)

    updateInput()
}

fun update(){
    val clients = TS3.clients()

    for (client in clients){
        processAfk(client)
    }
}

fun processAfk(client: Client){
    val config = ConfigFactory.config.afkManagementConfig

    val api = TS3.api
    if (!config.enabled || !TS3.channelExists(config.afkChannelId) || config.afkChannelId == client.channelId)
        return

    if (client.isAway || client.isOutputMuted && client.isInputMuted && client.idleTime / 1000 > config.afkDurationInSec)
        api.moveClient(client.id, config.afkChannelId)
}

fun updateInput(){
    while (true){
        val input = readLine()
        for(cmd in commands){
            if (input.equals(cmd.command)){
                cmd.func()
                break
            }
        }
    }
}