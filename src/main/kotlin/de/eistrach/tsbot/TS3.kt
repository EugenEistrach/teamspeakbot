package de.eistrach.tsbot

import com.github.theholywaffle.teamspeak3.TS3Api
import com.github.theholywaffle.teamspeak3.TS3Config
import com.github.theholywaffle.teamspeak3.TS3Query
import com.github.theholywaffle.teamspeak3.api.wrapper.Client
import de.eistrach.tsbot.config.ConfigFactory

class TS3 private constructor() {

    companion object {

        val api : TS3Api
        val query : TS3Query

        init{
            val config = ConfigFactory.config.serverConfig

            val ts3Config = TS3Config()
            ts3Config.setHost(config.ip)
            ts3Config.setFloodRate(TS3Query.FloodRate.UNLIMITED)

            query = TS3Query(ts3Config)
            query.connect()

            api = query.api

            api.login(config.username, config.password)
            api.selectVirtualServerById(config.virtualServerId)
            api.setNickname(config.nickname)

            api.registerAllEvents()
            api.addTS3Listeners(DefaultTS3Listener())


        }

        fun playerCount() : Int {
            return clients().size;
        }

        fun clients(): List<Client> {
            if (api.clients == null)
                return emptyList()

            return api.clients.filter { c -> !c.isServerQueryClient }
        }

        fun channelExists(channelId: Int) : Boolean {
            if (channelId == 0)
                return false;
            val info = api.getChannelInfo(channelId)
            return info != null;
        }
    }
}